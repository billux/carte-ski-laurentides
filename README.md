# Carte de ski des Laurentides

## Édition

Le projet a été créé à l'aide de l'outil libre [QGIS](https://qgis.org/) 3.16.

Voir https://univers-libre.net/posts/carte-ski-laurentides.html.

## Mise à jour des données OSM

En attendant d'arriver à mieux automatiser la procédure, il faut passer par QuickOSM depuis QGIS : Menu `Vector` → `QuickOSM` → `QuickOSM`.

Il faut mettre à jour les données de chaque layer :

- Layer `piste:type ways`  
  requête Overpass :
  ```
  <osm-script output="xml" timeout="25">
      <union>
          <query type="way">
              <has-kv k="piste:type" v="nordic"/>
              <bbox-query {{bbox}}/>
          </query>
      </union>
      <union>
          <item/>
          <recurse type="down"/>
      </union>
      <print mode="body"/>
  </osm-script>
  ```
  `Advanced` : sélectionner uniquement `Lines`.
- Layer `piste:type ways not part of a relation`  
  requête Overpass :
  ```
  <osm-script>
    <query into="all" type="way">
      <has-kv k="piste:type" modv="" v="nordic"/>
    </query>
    <query into="_" type="relation">
      <has-kv k="piste:type" modv="" v="nordic"/>
      <recurse from="all" type="way-relation"/>
    </query>
    <recurse type="relation-way" into="members"/>
    <difference into="_">
      <item from="all" into="_"/>
      <item from="members" into="_"/>
    </difference>
    <union>
      <item/>
      <recurse type="down"/>
    </union>
    <print e="" from="_" geometry="full" ids="yes" limit="" mode="body" n="" order="id" s="" w=""/>
  </osm-script>
  ```
  `Advanced` : sélectionner uniquement `Lines`.
- Layer `piste:type relations`  
  requête Overpass :
  ```
  <osm-script output="xml" timeout="25">
      <union>
          <query type="relation">
              <has-kv k="piste:type" v="nordic"/>
              <bbox-query {{bbox}}/>
          </query>
      </union>
      <union>
          <item/>
          <recurse type="down"/>
      </union>
      <print mode="body"/>
  </osm-script>
  ```
  `Advanced` : sélectionner `Multilinestrings` et `Multipolygons`.
- Layer `highway=path ways`  
  requête Overpass :
  ```
  <osm-script output="xml" timeout="25">
      <union>
          <query type="way">
              <has-kv k="highway" v="path"/>
              <bbox-query {{bbox}}/>
          </query>
      </union>
      <union>
          <item/>
          <recurse type="down"/>
      </union>
      <print mode="body"/>
  </osm-script>
  ```
  `Advanced` : sélectionner uniquement `Lines`.

La `{{bbox}}` doit correspondre à `(45.789551885662,-74.702911376953,46.373464301373,-73.920135498047)` (ou utiliser l'étendue du canvas).

Chaque requête va créer un nouveau layer temporaire dans le projet. Le sauvegarder (clic droit, `Make permanent`) dans le `.geojson` correspondant à chaque layer (ces fichiers GeoJSON sont configurés comme source de données des 4 layers), puis supprimer chaque layer QuickOSM. Fermer/réouvrir le projet pour prendre en compte les modifications.

Ensuite ouvrir chaque layout (secteur nord & secteur sud) dans `Project` → `Layouts`. Vérifier la vue, éditer la date de mise à jour dans l'encart pour information puis `Layout` → `Export as PDF`.

Enfin pour découper chaque PDF au format A4 (ou autre), utiliser [posterazor](https://sourceforge.net/projects/posterazor/).
